'use client'
import React from 'react'
import { Metadata } from 'next'
import Link from 'next/link'
import { usePathname, useRouter } from 'next/navigation'

// export const metadata : Metadata = {
//     title:"Dashboard",
//     description:"My project next js"
// }

const Layout = ({children}:{children:React.ReactNode}) => {
    const router = useRouter()
    const pathname = usePathname()
  return (
    <>
        <header className='p-5 bg-indigo-300'>
            <h1 className='text-center'>Header</h1>
        </header>

         <div>
        <div className='flex gap-5 my-3'>
            <Link 
            href={'/dashboard/settings'}
            className={`btn btn-primary ${pathname == "/dashboard/settings" && "bg-blue-700"}`}
            >
                settings
            </Link>
            <Link 
            href={'/dashboard/settings'}
            className={`btn-primary ${pathname == "/dashboard/finance" && "bg-blue-700"}`}
            >
                Finance
            </Link>
            <button
            onClick={() => router.push("/dashboard/invoices")}
            className={`btn-primary ${pathname == "/dashboard/invoices" && "bg-blue-700"}`}
            >
                Invoices
            </button>
        </div>
        <h1>
        Dashboard Page
        </h1>
    </div>
        <main className='p-5 bg-slate-300'>
            <h1 className='text-center'>{children}</h1>
        </main>
        <footer className='p-5 bg-red-200'>
            <h1 className='text-center'>Footer</h1>
        </footer>
    </>
  )
}

export default Layout