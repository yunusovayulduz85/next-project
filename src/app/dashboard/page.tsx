import Link from 'next/link'
import React from 'react'

const Dashboard = () => {
  return (
    <div>
        <div className='flex gap-5'>
            <Link href={'/settings'}>settings</Link>
        </div>
        <h1>
        Dashboard Page
        </h1>
    </div>
  )
}

export default Dashboard