import React from 'react'

const FeaturesSlug = ({params}:any) => {
  return (
    <div>
        Catch All Segments Page <b>{params.slug.join("/")}</b>
    </div>
  )
}

export default FeaturesSlug