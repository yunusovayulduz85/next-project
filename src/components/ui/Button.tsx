import React from 'react'

const DifferentButton = () => {
  return (
    <button className='btn-primary'>
        Different Button
    </button>
  )
}

export default DifferentButton